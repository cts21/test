package com.cts.proj.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class ApplicationConfig {
	@Bean
	public Employee employee() {
		Employee ee=new Employee();
		ee.setId(10);
		return ee;
	}
	
}
