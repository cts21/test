package com.cts.proj.SampleProject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cts.proj.model.ApplicationConfig;
import com.cts.proj.model.Employee;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	AnnotationConfigApplicationContext ac=new AnnotationConfigApplicationContext(ApplicationConfig.class);
//      ApplicationContext ctx=new ClassPathXmlApplicationContext("config.xml");
        Employee ee=(Employee)ac.getBean(Employee.class);
        System.out.println(ee.getId());
    }
}
